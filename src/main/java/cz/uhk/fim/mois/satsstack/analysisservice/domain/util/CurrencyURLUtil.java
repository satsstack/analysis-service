package cz.uhk.fim.mois.satsstack.analysisservice.domain.util;

/**
 * @author : vanya.melnykovych
 * @since : 25.04.2021
 */
public class CurrencyURLUtil {

    public static final String CURRENCY_API_BASE_URL = "https://v6.exchangerate-api.com/";
    public static final String CURRENCY_API_VERSION = "/v6";
    public static final String CURRENCY_API_LATEST_USD_INFO_ENDPOINT = "/latest/USD";
}
