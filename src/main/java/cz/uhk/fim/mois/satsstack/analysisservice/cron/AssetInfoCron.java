package cz.uhk.fim.mois.satsstack.analysisservice.cron;

import cz.uhk.fim.mois.satsstack.analysisservice.domain.Asset;
import cz.uhk.fim.mois.satsstack.analysisservice.domain.AssetJsonObject;
import cz.uhk.fim.mois.satsstack.analysisservice.service.api.CoinMarketAPIService;
import cz.uhk.fim.mois.satsstack.analysisservice.service.JsonManagingService;
import cz.uhk.fim.mois.satsstack.analysisservice.service.TransactionServiceRequestService;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 28.04.2021
 */
@Component
@EnableScheduling
public class AssetInfoCron {

    private final CoinMarketAPIService coinMarketAPIService;

    private final JsonManagingService jsonManagingService;

    private final TransactionServiceRequestService transactionServiceRequestService;

    public AssetInfoCron(CoinMarketAPIService coinMarketAPIService, JsonManagingService jsonManagingService, TransactionServiceRequestService transactionServiceRequestService) {
        this.coinMarketAPIService = coinMarketAPIService;
        this.jsonManagingService = jsonManagingService;
        this.transactionServiceRequestService = transactionServiceRequestService;
    }

    @Scheduled(cron = "0 0 0 * * *")
    public void appendCurrentAssetPrice() {
        List<Asset> assets = transactionServiceRequestService.getAllAssets();
        assets.forEach(a -> {
            double currentAssetPrice = coinMarketAPIService.getCurrentPriceForAsset(a.getName());
            List<AssetJsonObject> assetJsonObjects = jsonManagingService.getAssetFromJson(a.getShortCut());
            assetJsonObjects.add(new AssetJsonObject(new Date(), currentAssetPrice));
            jsonManagingService.writeData(assetJsonObjects, a.getShortCut());
        });
    }
}
