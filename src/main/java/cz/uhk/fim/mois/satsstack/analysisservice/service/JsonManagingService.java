package cz.uhk.fim.mois.satsstack.analysisservice.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import cz.uhk.fim.mois.satsstack.analysisservice.domain.AssetJsonObject;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 27.04.2021
 */
@Service
public class JsonManagingService {

    private static final String HISTORICAL_FILE_LOCATION = "src/main/resources/static/";
    private static final String HISTORICAL_FILE_NAME = "-info-historical.json";

    public List<AssetJsonObject> getAssetFromJson(String assetShortCut) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Type dtoListType = new TypeToken<List<AssetJsonObject>>(){}.getType();
        List<AssetJsonObject> result = new ArrayList<>();
        try (FileReader fr = new FileReader(HISTORICAL_FILE_LOCATION + assetShortCut.toLowerCase() + HISTORICAL_FILE_NAME)) {
            result = gson.fromJson(fr, dtoListType);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public void writeData(List<AssetJsonObject> assetJsonObjects, String assetShortCut) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        try (FileWriter fw = new FileWriter(HISTORICAL_FILE_LOCATION + assetShortCut.toLowerCase() + HISTORICAL_FILE_NAME)) {
            gson.toJson(assetJsonObjects, fw);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
