package cz.uhk.fim.mois.satsstack.analysisservice.domain;

import cz.uhk.fim.mois.satsstack.analysisservice.domain.util.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author : vanya.melnykovych
 * @since : 22.04.2021
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {

    private Long id;

    private double fee;

    private double quantity;

    private String note;

    private TransactionType transactionType;

    private BigDecimal pricePerAsset;

    private Date date;

    private String userId;

    private Asset asset;

    public List<String> toCsvRow() {
        return Stream.of(asset.getName(), pricePerAsset.doubleValue(), transactionType, quantity, fee, date, note)
                .map(String::valueOf)
                .collect(Collectors.toList());
    }
}
