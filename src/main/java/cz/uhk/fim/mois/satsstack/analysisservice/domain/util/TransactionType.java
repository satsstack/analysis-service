package cz.uhk.fim.mois.satsstack.analysisservice.domain.util;

/**
 * @author : vanya.melnykovych
 * @since : 22.04.2021
 */
public enum TransactionType {
    BUY,
    SELL
}
