package cz.uhk.fim.mois.satsstack.analysisservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : vanya.melnykovych
 * @since : 22.04.2021
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response {

    private Long id;

    private String name;

    private double price;
}
