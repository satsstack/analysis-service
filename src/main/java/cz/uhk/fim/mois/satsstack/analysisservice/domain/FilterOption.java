package cz.uhk.fim.mois.satsstack.analysisservice.domain;

import cz.uhk.fim.mois.satsstack.analysisservice.domain.util.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 23.04.2021
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilterOption {

    private Date start;

    private Date end;

    private List<Asset> assets;

    private TransactionType transactionType;

}
