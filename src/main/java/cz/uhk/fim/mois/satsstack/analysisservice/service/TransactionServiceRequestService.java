package cz.uhk.fim.mois.satsstack.analysisservice.service;

import cz.uhk.fim.mois.satsstack.analysisservice.domain.Asset;
import cz.uhk.fim.mois.satsstack.analysisservice.domain.Transaction;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;
import javax.net.ssl.HttpsURLConnection;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author : vanya.melnykovych
 * @since : 28.04.2021
 */
@Service
public class TransactionServiceRequestService {


    @Value("${transaction.service.host.name}")
    private String transactionServiceHostName;

    @Value("${transaction.service.host.port}")
    private String transactionServiceHostPort;

    private final String TRANSACTION_SERVICE_CONTROLLER_URL = "rest/api/transaction";

    private final String TRANSACTION_SERVICE_ASSET_CONTROLLER_URL = "rest/api/asset";

    public List<Transaction> getUserTransactions(String userId, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);
        HttpEntity<Boolean> request = new HttpEntity<>(headers);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(buildURLForFindingUserTransactions()
                        + TRANSACTION_SERVICE_CONTROLLER_URL
                        + "/find/"
                        + userId);
        ResponseEntity<Transaction[]> response = new RestTemplate()
                .exchange(uriBuilder.toUriString(), HttpMethod.GET, request, Transaction[].class);
        return Arrays.asList(Objects.requireNonNull(response.getBody()));
    }

    public List<Transaction> getUserTransactionsByAsset(String userId, Long assetId, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);
        HttpEntity<Boolean> request = new HttpEntity<>(headers);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(buildURLForFindingUserTransactions()
                        + TRANSACTION_SERVICE_CONTROLLER_URL
                        + "/find/asset/"
                        + userId + "/" + assetId);
        ResponseEntity<Transaction[]> response = new RestTemplate()
                .exchange(uriBuilder.toUriString(), HttpMethod.GET, request, Transaction[].class);
        return Arrays.asList(Objects.requireNonNull(response.getBody()));
    }

    public List<Asset> getAllAssets() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<Asset> request = new HttpEntity<>(headers);
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromHttpUrl(buildURLForFindingUserTransactions()
                        + TRANSACTION_SERVICE_ASSET_CONTROLLER_URL
                        + "/all");
        ResponseEntity<Asset[]> response = new RestTemplate()
                .exchange(uriBuilder.toUriString(), HttpMethod.GET, request, Asset[].class);
        return Arrays.asList(Objects.requireNonNull(response.getBody()));
    }

    private String buildURLForFindingUserTransactions() {
        StringBuilder builder = new StringBuilder()
                .append("https://")
                .append(transactionServiceHostName);
        if (transactionServiceHostPort != null && !transactionServiceHostPort.isEmpty()) {
            builder.append(":")
                    .append(transactionServiceHostPort);
        }
        builder.append("/");
        return builder.toString();
    }

    @PostConstruct
    private void setDefaultHostnameVerifier() {
        HttpsURLConnection.setDefaultHostnameVerifier((s, sslSession) -> true);
    }

}
