package cz.uhk.fim.mois.satsstack.analysisservice.config;

import cz.uhk.fim.mois.satsstack.analysisservice.filter.TokenRequestFilter;
import cz.uhk.fim.mois.satsstack.analysisservice.token.TokenAuthenticationEntryPoint;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.rememberme.RememberMeAuthenticationFilter;

/**
 * @author : vanya.melnykovych
 * @since : 27.04.2021
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final TokenRequestFilter tokenRequestFilter;

    private final TokenAuthenticationEntryPoint tokenAuthenticationEntryPoint;

    public WebSecurityConfig(TokenRequestFilter tokenRequestFilter, TokenAuthenticationEntryPoint tokenAuthenticationEntryPoint) {
        this.tokenRequestFilter = tokenRequestFilter;
        this.tokenAuthenticationEntryPoint = tokenAuthenticationEntryPoint;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .csrf().disable()
                .authorizeRequests().antMatchers("/**/all",
                "/info",
                "/health",
                "/health/**",
                "/v3/api-docs",
                "/v3/api-docs/**",
                "/v3/api-docs/swagger-config",
                "/swagger-ui/oauth2-redirect.html",
                "/swagger-ui.html",
                "/swagger-ui/**",
                "/rest/api/statistics/export/**")
                .permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(tokenRequestFilter, RememberMeAuthenticationFilter.class)
                .exceptionHandling().authenticationEntryPoint(tokenAuthenticationEntryPoint)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
