package cz.uhk.fim.mois.satsstack.analysisservice.service;

import cz.uhk.fim.mois.satsstack.analysisservice.domain.*;
import cz.uhk.fim.mois.satsstack.analysisservice.domain.util.TransactionType;
import cz.uhk.fim.mois.satsstack.analysisservice.service.api.CoinMarketAPIService;
import cz.uhk.fim.mois.satsstack.analysisservice.service.api.CurrencyApiService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.javatuples.Triplet;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author : vanya.melnykovych
 * @since : 22.04.2021
 */
@Service
public class StatisticService {

    private final CurrencyApiService currencyApiService;

    private final CoinMarketAPIService coinMarketApiService;

    private final JsonManagingService jsonManagingService;

    private final TransactionServiceRequestService transactionServiceRequestService;

    public StatisticService(CurrencyApiService currencyApiService, CoinMarketAPIService coinMarketApiService, JsonManagingService jsonManagingService, TransactionServiceRequestService transactionServiceRequestService) {
        this.currencyApiService = currencyApiService;
        this.coinMarketApiService = coinMarketApiService;
        this.jsonManagingService = jsonManagingService;
        this.transactionServiceRequestService = transactionServiceRequestService;
    }

    public ResponseEntity<Resource> doExport(String userId, String token, String currency, FilterOption filterOption) {
        List<Transaction> transactions = getFilteredTransaction(userId, token, filterOption);
        List<Asset> userAssets = getUserAssets(transactions);
        List<ViewAsset> viewAssets = mergeAllData(transactions, userAssets, userId, token, currency);
        double currencyValue = getCurrencyValue(currency);
        String[] csvHeaders =  { "Asset name", "Current price", "Holding value", "Holding quantity", "Profit/Loss value", "Profit/Loss %"};
        List<String> transactionHeader = Arrays.asList("Asset name : ", "Buy price", "Transaction type", "Quantity", "Fee", "Date", "Note");
        String detail = "------------------------------------ Transactions ------------------------------------";
        String end = "--------------------------------------------------------------------------------------";

        ByteArrayInputStream byteArrayOutputStream;
        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                CSVPrinter csvPrinter = new CSVPrinter(
                        new PrintWriter(out),
                        CSVFormat.DEFAULT.withHeader(csvHeaders)
                );
        ) {
            for (ViewAsset viewAsset : viewAssets) {
                csvPrinter.printRecord(viewAsset.toCsvRow());
                csvPrinter.printRecord(detail);
                csvPrinter.printRecord(transactionHeader);
                transactions.stream()
                        .filter(t -> t.getAsset().getName().equals(viewAsset.getAsset().getName()))
                        .forEach(t -> {
                            try {
                                t.setPricePerAsset(BigDecimal.valueOf(t.getPricePerAsset().doubleValue() * currencyValue));
                                t.setFee(t.getFee() * currencyValue);
                                csvPrinter.printRecord(t.toCsvRow());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                csvPrinter.printRecord(end);
            }
            csvPrinter.flush();
            byteArrayOutputStream = new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }

        InputStreamResource fileInputStream = new InputStreamResource(byteArrayOutputStream);

        final String csvFileName = "statistics-" + userId + "-" + LocalDate.now() + ".csv";

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + csvFileName);
        headers.set(HttpHeaders.CONTENT_TYPE, "text/csv");

        return new ResponseEntity<>(
                fileInputStream,
                headers,
                HttpStatus.OK
        );
    }

    public List<Transaction> getFilteredTransaction(String userId, String token, FilterOption filterOption) {
        List<Transaction> transactions = transactionServiceRequestService.getUserTransactions(userId, token);
        if (filterOption != null) {
            if (filterOption.getStart() != null) {
                if (filterOption.getEnd() == null) {
                    filterOption.setEnd(new Date());
                }
                transactions = transactions.stream()
                        .filter(t -> t.getDate().after(filterOption.getStart())
                                        && t.getDate().before(filterOption.getEnd()))
                        .collect(Collectors.toList());
            } else if (filterOption.getEnd() != null) {
                transactions = transactions.stream()
                        .filter(t -> t.getDate().before(filterOption.getEnd()))
                        .collect(Collectors.toList());
            }
            if (filterOption.getAssets() != null && !filterOption.getAssets().isEmpty()) {
                transactions = transactions.stream()
                        .filter(t -> filterOption.getAssets().contains(t.getAsset()))
                        .collect(Collectors.toList());
            }
            if (filterOption.getTransactionType() != null) {
                transactions = transactions.stream()
                        .filter(t -> t.getTransactionType() == filterOption.getTransactionType())
                        .collect(Collectors.toList());
            }
        }
        return transactions;
    }

    public List<ViewAsset> getViewAssets(String userId, String token, String currency) {
        final List<Transaction> transactions = transactionServiceRequestService.getUserTransactions(userId, token);
        final List<Asset> userAssets = getUserAssets(transactions);
        return mergeAllData(transactions, userAssets, userId, token, currency);
    }

    private List<ViewAsset> mergeAllData(List<Transaction> transactions, List<Asset> assets, String userId, String token, String currency) {
        final double currencyValue = getCurrencyValue(currency);
        return assets.stream().map(a -> {
            final ViewAsset viewAsset = new ViewAsset();
            final String assetName = a.getName();
            List<Transaction> assetTransactions = transactions.stream()
                    .filter(t -> t.getAsset().getName().equals(assetName))
                    .collect(Collectors.toList());
            final double currentAssetPrice = coinMarketApiService.getCurrentPriceForAsset(assetName) * currencyValue;
            final double holdingQuantity = getAssetHoldingQuantity(assetTransactions);
            final double holdingValue = holdingQuantity * currentAssetPrice;
            final double profitLossValue = (calculateProfitLossValue(assetTransactions) * currencyValue) + holdingValue;
            final double profitLossPercentage = profitLossValue / calculateInvested(assetTransactions, currency) * 100;

            viewAsset.setAsset(a);
            viewAsset.setPrice(currentAssetPrice);
            viewAsset.setHoldingValue(holdingValue);
            viewAsset.setHoldingQuantity(holdingQuantity);
            viewAsset.setProfitLossValue(profitLossValue);
            viewAsset.setProfitLossPercentage(profitLossPercentage);
            return viewAsset;
        }).collect(Collectors.toList());
    }
    
    private double getAssetHoldingQuantity(List<Transaction> transactions) {
        return transactions.stream()
                .filter(t -> t.getTransactionType()== TransactionType.BUY)
                .mapToDouble(Transaction::getQuantity)
                .sum() - transactions.stream()
                .filter(t -> t.getTransactionType()== TransactionType.SELL)
                .mapToDouble(Transaction::getQuantity)
                .sum();
    }

    private double calculateProfitLossValue(List<Transaction> transactions) {
        return transactions.stream().mapToDouble(t -> {
             if (t.getTransactionType() == TransactionType.BUY) {
                 return -(t.getQuantity() * t.getPricePerAsset().doubleValue());
             } else if (t.getTransactionType() == TransactionType.SELL) {
                 return t.getQuantity() * t.getPricePerAsset().doubleValue();
             }
             return 0.0;
        }).sum();
    }

    public double calculateAllInvested(String userId, String token, String currency) {
        List<Transaction> transactions = transactionServiceRequestService.getUserTransactions(userId, token);
        return calculateInvested(transactions, currency);
    }

    public double calculateAllSellValue(String userId, String token, String currency) {
        List<Transaction> transactions = transactionServiceRequestService.getUserTransactions(userId, token);
        return calculateSellValue(transactions, currency);
    }

    public double calculateSellValueForAsset(String userId, Long assetId, String token, String currency) {
        List<Transaction> transactions = transactionServiceRequestService.getUserTransactionsByAsset(userId, assetId, token);
        return calculateSellValue(transactions, currency);
    }

    private double calculateSellValue(List<Transaction> transactions, String currency) {
        double currencyValue = getCurrencyValue(currency);
        return transactions.stream()
                .filter(t -> t.getTransactionType() == TransactionType.SELL)
                .mapToDouble(t -> t.getPricePerAsset().doubleValue() * t.getQuantity())
                .sum() * currencyValue;
    }

    public double getInvestedByAsset(String userId, Long assetId, String token, String currency) {
        List<Transaction> transactions = transactionServiceRequestService.getUserTransactionsByAsset(userId, assetId, token);
        return calculateInvested(transactions, currency);
    }

    private double calculateInvested(List<Transaction> transactions, String currency) {
        double currencyValue = getCurrencyValue(currency);
        return transactions.stream()
                .filter(t -> t.getTransactionType() == TransactionType.BUY)
                .mapToDouble(t -> t.getPricePerAsset().doubleValue() * t.getQuantity())
                .sum() * currencyValue;
    }

    public Map<String, Double> getHoldingGraphValues(String userId, String token) {
        List<Transaction> transactions = transactionServiceRequestService.getUserTransactions(userId, token);

        final double totalHoldingValue = transactions.stream()
                .mapToDouble(t -> {
                    return t.getQuantity() * coinMarketApiService.getCurrentPriceForAsset(t.getAsset().getName());
                }).sum();
        final List<Asset> userAssets = getUserAssets(transactions);
        return userAssets.stream()
                .collect(Collectors.toMap(Asset::getShortCut, a -> {
                    final double currentPrice = coinMarketApiService.getCurrentPriceForAsset(a.getName());
                    final double holdingValue = transactions.stream()
                            .filter(t -> t.getAsset().getName().equals(a.getName()))
                            .mapToDouble(t -> {
                                return t.getQuantity() * currentPrice;
                            }).sum();
                    return holdingValue / totalHoldingValue * 100;
                }));
    }

    public List<Triplet<Date, Double, Double>> getLineChartValues(String userId, String token, Long assetId, String currency) {
        List<Transaction> transactions;
        if (assetId != null) {
            transactions = transactionServiceRequestService.getUserTransactionsByAsset(userId, assetId, token);
        } else {
            transactions = transactionServiceRequestService.getUserTransactions(userId, token);
        }
        return calculateLineChart(transactions, userId, token, currency);
    }

    private List<Triplet<Date, Double, Double>> calculateLineChart(List<Transaction> transactions, String userId, String token, String currency) {
        List<Triplet<Date, Double, Double>> result = new ArrayList<>();
        final double currencyValue = getCurrencyValue(currency);
        Date endGraphDate = addOneDay(new Date());
        Date graphDate = getFirstGraphDay(endGraphDate);

        while(graphDate.before(endGraphDate)) {
            Date currentGraphDate = graphDate;
            List<Transaction> currentTransactions = transactions.stream()
                    .filter(t -> t.getDate().before(currentGraphDate))
                    .collect(Collectors.toList());

            if (currentTransactions.size() == 0) {
                result.add(Triplet.with(currentGraphDate, 0.0, 0.0));
            } else {
                double holdingValue = calculateHoldingValue(currentTransactions, currentGraphDate) * currencyValue;
                double invested = calculateInvestedForLineChart(currentTransactions) * currencyValue;
                result.add(Triplet.with(currentGraphDate, holdingValue, Math.max(0, invested)));
            }
            graphDate = addOneDay(graphDate);
        }
        return result;
    }

    private double calculateHoldingValue(List<Transaction> transactions, Date currentGraphDate) {
        return transactions.stream().mapToDouble(t -> {
            if (checkDate(t.getDate(), currentGraphDate)) {
                final double value = t.getQuantity() * t.getPricePerAsset().doubleValue();
                return t.getTransactionType() == TransactionType.BUY ? value : -value;
            }
            List<AssetJsonObject> assetJsonObjects =
                    jsonManagingService.getAssetFromJson(t.getAsset().getShortCut());
            final double priceValue = assetJsonObjects.stream()
                    .filter(a -> checkDate(a.getDate(), currentGraphDate))
                    .findFirst().orElse(new AssetJsonObject(currentGraphDate, 1.0))
                    .getPrice() * t.getQuantity();
            return t.getTransactionType() == TransactionType.BUY ? priceValue : -priceValue;
        }).sum();
    }

    private double calculateInvestedForLineChart(List<Transaction> transactions) {
        return transactions.stream().mapToDouble(t -> {
            final double value = t.getQuantity() * t.getPricePerAsset().doubleValue();
            return t.getTransactionType() == TransactionType.BUY ? value : -value;
        }).sum();
    }

    private Date getFirstGraphDay(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, -90);
        return cal.getTime();
    }

    private Date addOneDay(Date date) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        return cal.getTime();
    }

    private boolean checkDate(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
    }

    private double calculateTransactionQuantity(Transaction transaction) {
        if (transaction.getTransactionType() == TransactionType.BUY) {
            return transaction.getQuantity();
        } else if (transaction.getTransactionType() == TransactionType.SELL) {
            return -transaction.getQuantity();
        }
        return 0.0;
    }

    public double getCurrencyValue(String currency) {
        if (currency == null || currency.isEmpty()) {
            return 1.0;
        }
        if (currency.equals("BTC")) {
            double bitCoinPrice = coinMarketApiService.getCurrentPriceForAsset("Bitcoin");
            return 1.0 / bitCoinPrice;
        }
        return currencyApiService.getCurrency(currency);
    }

    public double getCurrentPrice(String assetName, String currency) {
        double currencyValue = getCurrencyValue(currency);
        return coinMarketApiService.getCurrentPriceForAsset(assetName) * currencyValue;
    }

    private List<Asset> getUserAssets(List<Transaction> transactions) {
        return transactions.stream()
                .filter(distinctByKey(t -> t.getAsset().getId()))
                .map(Transaction::getAsset)
                .collect(Collectors.toList());
    }

    private <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }
}
