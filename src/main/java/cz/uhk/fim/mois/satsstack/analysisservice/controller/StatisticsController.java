package cz.uhk.fim.mois.satsstack.analysisservice.controller;

import cz.uhk.fim.mois.satsstack.analysisservice.domain.FilterOption;
import cz.uhk.fim.mois.satsstack.analysisservice.domain.ViewAsset;
import cz.uhk.fim.mois.satsstack.analysisservice.service.StatisticService;
import org.javatuples.Triplet;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author : vanya.melnykovych
 * @since : 22.04.2021
 */
@RestController
@RequestMapping("rest/api/statistics")
public class StatisticsController {

    private final StatisticService statisticService;

    public StatisticsController(StatisticService statisticService) {
        this.statisticService = statisticService;
    }

    @GetMapping("/view/asset/{userId}")
    public List<ViewAsset> getViewAssets(@PathVariable String userId, @RequestHeader("Authorization") String token,
                                         @RequestParam(required = false) String currency) {
        return statisticService.getViewAssets(userId, token, currency);
    }

    @GetMapping("/invested/{userId}")
    public double getTotalInvested(@PathVariable String userId, @RequestHeader("Authorization") String token,
                                   @RequestParam(required = false) String currency) {
        return statisticService.calculateAllInvested(userId, token, currency);
    }

    @GetMapping("/invested/asset/{userId}/{assetId}")
    public double getTotalInvestedByAsset(@PathVariable String userId, @PathVariable Long assetId,
                                          @RequestHeader("Authorization") String token,
                                          @RequestParam(required = false) String currency) {
        return statisticService.getInvestedByAsset(userId, assetId, token, currency);
    }

    @GetMapping("/sell/{userId}")
    public double getTotalSellValue(@PathVariable String userId,
                                    @RequestHeader("Authorization") String token,
                                    @RequestParam(required = false) String currency) {
        return statisticService.calculateAllSellValue(userId, token, currency);
    }

    @GetMapping("/sell/{userId}/{assetId}")
    public double getTotalSellValue(@PathVariable String userId,
                                    @PathVariable Long assetId,
                                    @RequestHeader("Authorization") String token,
                                    @RequestParam(required = false) String currency) {
        return statisticService.calculateSellValueForAsset(userId, assetId, token, currency);
    }

    @GetMapping("/graph/holding/{userId}")
    public Map<String, Double> getHoldingGraphValues(@PathVariable String userId, @RequestHeader("Authorization") String token) {
        return statisticService.getHoldingGraphValues(userId, token);
    }

    @GetMapping("/price/{assetName}")
    public double getCurrentPrice(@PathVariable String assetName,
                                  @RequestParam(required = false) String currency) {
        return statisticService.getCurrentPrice(assetName, currency);
    }

    @GetMapping(value = "/export/{userId}", produces = "text/csv")
    public ResponseEntity<Resource> exportCSV(@PathVariable String userId, @RequestParam String token,
                                              @RequestBody(required = false) FilterOption filterOption,
                                              @RequestParam(required = false) String currency) {
        return statisticService.doExport(userId, token, currency, filterOption);
    }

    @GetMapping("/currency")
    public double getCurrency(@RequestParam String currency) {
        return statisticService.getCurrencyValue(currency);
    }

    @GetMapping("/graph/line/{userId}")
    public List<Triplet<Date, Double, Double>> getLineChartValues(@PathVariable String userId,
                                                                  @RequestHeader("Authorization") String token,
                                                                  @RequestParam(required = false) Long assetId,
                                                                  @RequestParam(required = false) String currency) {
        return statisticService.getLineChartValues(userId, token, assetId, currency);
    }
}
