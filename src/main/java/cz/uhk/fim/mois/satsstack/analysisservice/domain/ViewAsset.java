package cz.uhk.fim.mois.satsstack.analysisservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author : vanya.melnykovych
 * @since : 22.04.2021
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViewAsset {

    private Asset asset;

    private double price;

    private double holdingValue;

    private double holdingQuantity;

    private double profitLossValue;

    private double profitLossPercentage;

    public List<String> toCsvRow() {
        return Stream.of(asset.getName(), price, holdingValue, holdingQuantity, profitLossValue, profitLossPercentage)
                .map(String::valueOf)
                .collect(Collectors.toList());
    }
}
