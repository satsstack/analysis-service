package cz.uhk.fim.mois.satsstack.analysisservice.domain.util;

/**
 * @author : vanya.melnykovych
 * @since : 22.04.2021
 */
public class CoinMarketURLUtil {

    public static final String COIN_MARKET_API_BASE_URL = "https://pro-api.coinmarketcap.com";
    public static final String COIN_MARKET_API_VERSION = "/v1";
    public static final String COIN_MARKET_API_CRYPTOCURRENCY_ENDPOINT = "/cryptocurrency";
    public static final String COIN_MARKET_API_LATEST_INFO_ENDPOINT = "/listings/latest";

}
