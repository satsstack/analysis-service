package cz.uhk.fim.mois.satsstack.analysisservice.service.api;

import cz.uhk.fim.mois.satsstack.analysisservice.domain.Response;
import cz.uhk.fim.mois.satsstack.analysisservice.domain.util.CoinMarketURLUtil;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author : vanya.melnykovych
 * @since : 22.04.2021
 */
@Service
public class CoinMarketAPIService {

    @Value("${coin.market.api-key}")
    private String apiKey;

    public String getCryptocurrencyLatestInfo() {
        final String uri = CoinMarketURLUtil.COIN_MARKET_API_BASE_URL + CoinMarketURLUtil.COIN_MARKET_API_VERSION
                + CoinMarketURLUtil.COIN_MARKET_API_CRYPTOCURRENCY_ENDPOINT + CoinMarketURLUtil.COIN_MARKET_API_LATEST_INFO_ENDPOINT;
        List<NameValuePair> parameters = new ArrayList<>();
        parameters.add(new BasicNameValuePair("start","1"));
        parameters.add(new BasicNameValuePair("limit","20"));
        parameters.add(new BasicNameValuePair("convert","USD"));

        String result = "";
        try {
            result = makeAPICall(uri, parameters);
        } catch (IOException e) {
            System.out.println("Error: cannont access content - " + e.toString());
            e.printStackTrace();
        } catch (URISyntaxException e) {
            System.out.println("Error: Invalid URL " + e.toString());
            e.printStackTrace();
        }
        return result;
    }

    private String makeAPICall(String uri, List<NameValuePair> parameters)
            throws URISyntaxException, IOException {
        String response_content = "";

        URIBuilder query = new URIBuilder(uri);
        query.addParameters(parameters);

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet request = new HttpGet(query.build());

        request.setHeader(HttpHeaders.ACCEPT, "application/json");
        request.addHeader("X-CMC_PRO_API_KEY", apiKey);

        CloseableHttpResponse response = client.execute(request);

        try {
            HttpEntity entity = response.getEntity();
            response_content = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }

        return response_content;
    }

    public double getCurrentPriceForAsset(String assetName) {
        String json = getCryptocurrencyLatestInfo();

        return (extractResponseObjectFromJson(json).stream()
                .filter(r -> r.getName().equals(assetName))
                .findFirst()
                .orElseThrow()
        ).getPrice();
    }

    private List<Response> extractResponseObjectFromJson(String json) {
        List<Response> responseList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                Response response = new Response();
                response.setId(jsonArray.getJSONObject(i).getLong("id"));
                response.setName(jsonArray.getJSONObject(i).getString("name"));
                response.setPrice(jsonArray.getJSONObject(i).getJSONObject("quote")
                        .getJSONObject("USD")
                        .getDouble("price"));
                responseList.add(response);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return responseList;
    }
}
