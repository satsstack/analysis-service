package cz.uhk.fim.mois.satsstack.analysisservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author : vanya.melnykovych
 * @since : 27.04.2021
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssetJsonObject {

    private Date date;

    private Double price;
}
