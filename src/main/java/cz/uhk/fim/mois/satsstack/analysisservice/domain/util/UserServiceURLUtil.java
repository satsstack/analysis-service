package cz.uhk.fim.mois.satsstack.analysisservice.domain.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author : vanya.melnykovych
 * @since : 27.04.2021
 */
@Component
public class UserServiceURLUtil {

    @Value("${user.service.host.name}")
    private String userServiceHost;

    @Value("${user.service.host.port}")
    private String userServicePort;

    public static final String USER_CONTROLLER_URL = "rest/api/user";

    public String buildUserServiceBaseURL() {
        StringBuilder builder = new StringBuilder()
                .append("https://")
                .append(userServiceHost);
        if (userServicePort != null && !userServicePort.isEmpty()) {
            builder.append(":")
                    .append(userServicePort);
        }
        builder.append("/");
        return builder.toString();
    }
}
