package cz.uhk.fim.mois.satsstack.analysisservice.service.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.uhk.fim.mois.satsstack.analysisservice.domain.util.CurrencyURLUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

/**
 * @author : vanya.melnykovych
 * @since : 25.04.2021
 */
@Service
public class CurrencyApiService {

    @Value("${currency.api-key}")
    private String apiKey;

    public double getCurrency(String currency) {
        double result = 1.0;
        final String json = getCurrencies();
        try {
            JSONObject jsonObject = new JSONObject(json);
            HashMap<String, Double> values = new ObjectMapper()
                    .readValue(jsonObject.get("conversion_rates").toString(), HashMap.class);
            result = values.get(currency);
        } catch (JSONException | JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    private String getCurrencies() {
        String result = "";
        try {
            result = makeApiCall();
        } catch (IOException e) {
            System.out.println("Error: cannont access content - " + e.toString());
            e.printStackTrace();
        } catch (URISyntaxException e) {
            System.out.println("Error: Invalid URL " + e.toString());
            e.printStackTrace();
        }
        return result;
    }

    private String makeApiCall() throws URISyntaxException, IOException {
        String responseContent = "";

        URIBuilder query = new URIBuilder(buildConnectionURL());

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet request = new HttpGet(query.build());
        CloseableHttpResponse response = client.execute(request);

        try {
            HttpEntity entity = response.getEntity();
            responseContent = EntityUtils.toString(entity);
            EntityUtils.consume(entity);
        } finally {
            response.close();
        }
        return responseContent;
    }

    private String buildConnectionURL() {
        return CurrencyURLUtil.CURRENCY_API_BASE_URL
                + CurrencyURLUtil.CURRENCY_API_VERSION
                + "/" + apiKey
                + CurrencyURLUtil.CURRENCY_API_LATEST_USD_INFO_ENDPOINT;
    }

}
